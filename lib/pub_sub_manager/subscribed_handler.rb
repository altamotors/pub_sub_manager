module PubSubManager
  class SubscribedHandler
    attr_reader :client, :data, :meta, :id, :subscription_key

    def initialize(client:, data:, meta:, id:, sub:)
      @client = client
      @data = data
      @meta = meta
      @id = id
      @subscription_key = sub
    end

    def publish(data, to:, meta: {})
      @client.publish(data, to: to, meta: meta.merge(@meta))
    end

    def switch_project_client(client)
      @client = client
    end
  end
end
