module PubSubManager
  class Client

    def self.for_subscription(name)
      full_match, project = *name.match(/\Aprojects\/([^\/]+)\//)
      project && for_project(project)
    end

    def self.for_project(project)
      @managers ||= Hash.new do |h, project|
        h[project] = new(setup_gcloud(project), project)
      end

      @managers[project]
    end

    def self.setup_gcloud(project)
      key, json = ENV.detect{|k,v| k.match(/JSON/) && v.include?(project) }
      Google::Cloud::Pubsub.new(
        project: project,
        keyfile: JSON.parse(json.gsub("\n","\\n")),
        timeout: 1
      )
    end

    attr_reader :pubsub, :topics

    def initialize(gcloud_client, project)
      @pubsub = Retrier.new(gcloud_client)
      @project = project

      @topics ||= Hash.new do |h, topic|
        loaded = load_topic(topic)
        h[topic] = loaded && Retrier.new(loaded)
      end
    end

    def verify_topic(name)
      topics[name] || create_topic(name)
    end

    def recreate_subscription(topic, name, endpoint = nil)
      subscription = topic.subscription(full_subscription(name))
      subscription.delete if subscription
      topic.subscribe(full_subscription(name), endpoint: endpoint)
    end

    def create_topic(name)
      @topics[name] = Retrier.new(pubsub.create_topic(full_topic(name)))
    end

    def publish(data, to:, meta: {})
      return if PubSubManager.skip_next_publish
      meta = meta.merge(PubSubManager.next_publish_meta || {})

      topics[to].publish(data.to_json, meta)

      PubSubManager.next_publish_meta = nil
      PubSubManager.skip_next_publish = nil
      return true
    end

    def full_topic(name)
      "projects/#{@project}/topics/#{name}"
    end

    def full_subscription(name)
      "projects/#{@project}/subscriptions/#{name}".gsub('%', subscription_separator)
    end

    private

    def subscription_separator
      Rails.application.config.subscription_separator
    rescue NoMethodError
      '%'
    end

    def _topic_name(topic)
      topic.name.match(/[^\/]*$/)[0]
    end

    def load_topic(name)
      pubsub.topic(full_topic(name))
    end

  end
end
