module PubSubManager
  # Many times a Google::Cloud::UnavailableError happens
  class Retrier
    def initialize(obj)
      @retryee = obj
    end

    def method_missing(method, *args)
      retries ||= 0
      @retryee.send(method, *args)
    rescue Exception => e
      Rails.logger.info('=== Got error, retrying ===')
      if retries < 3
        retries += 1
        sleep 1
        retry
      else
        Rollbar.error("Pubsub failed: #{method}, #{args}")
      end
    end
  end
end
