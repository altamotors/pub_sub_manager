module PubSubManager
  module PushHandler 
    def self.included(base)
      base.skip_before_action :verify_authenticity_token
      base.before_action :validate_api_key
      base.before_action :build_handler
    end

    def validate_api_key
      unless params[:api_key] == PubSubManager.api_key
        Rollbar.warning("Invalid API Key", params)
        head(403)
      end
    end

    def build_handler
      @handler = SubscribedHandler.new(
        client: Client.for_subscription(params[:subscription]),
        meta: params[:message][:attributes] || {},
        data: get_attributes,
        id: params[:message][:message_id],
        sub: params[:subscription],
      )
    end

    def get_attributes
      JSON.parse(Base64.decode64(params[:message][:data]))
    end
  end
end
