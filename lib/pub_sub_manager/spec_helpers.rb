def monitor_pubsub_logs(config)
  config.before :each, pubsubs: true do
    @pipe_stdin,
    @pipe_stdout,
    @wait_thr = Open3.popen2e('tail -n0 -f /container_logs/pubsub.log')
    @pid = @wait_thr.pid
  end

  config.after :each, pubsubs: true do
    Process.kill("HUP", @pid)
    bad_lines = @pipe_stdout.readlines.reject do |line|
      next true if line =~/\w{3}\s\d{1,2},\s\d{4}/ # date
      next true if line =~/Authentication interceptor/  
      next true if line =~/Adding handler/  
      next true if line =~/Detected HTTP/  
    end
    if bad_lines.any?
      raise bad_lines.join("\n")
    end
  end
end

def wait(time, increment = 5, elapsed_time = 0, &block)
  begin
    yield
  rescue Exception => e
    if elapsed_time >= time
      raise e
    else
      p e
      sleep increment
      wait(time, increment, elapsed_time + increment, &block)
    end
  end
end

def wait_for_pubsub_message(subscription, &block)
  wait(2, 0.2) do
    msg = subscription.pull(autoack: true).first
    raise 'no message' unless msg
    block.call(msg)
  end 
end
