require 'google/cloud/pubsub'
require 'pub_sub_manager/client'
require 'pub_sub_manager/push_handler'
require 'pub_sub_manager/subscribed_handler'
require 'pub_sub_manager/retrier'

module PubSubManager
  cattr_accessor :next_publish_meta, :skip_next_publish

  def self.api_key
    ENV['PUBSUB_API_KEY']
  end

end
